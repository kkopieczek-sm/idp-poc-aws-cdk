# AWS-ECS-CDK-DEMO
1. Running the pipeline = publishing app to AWS: run pipeline with `apply` variable.
2. Remove resources: run pipeline with `destroy` variable.
3. CDK IaC [here](infrastructure/lib/hello-ecs-stack.js)

## Local development

```bash
npm install -g aws-cdk
```